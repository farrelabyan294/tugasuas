    <!-- ======= About Section ======= -->
    <section class="about" id="about" style="background-color:orange;">

      <div class="container text-center">
        <h2>
          Pengertian Jamu
        </h2>
        Jamu adalah obat tradisional berbahan alami warisan budaya yang telah diwariskan secara turun-temurun dari generasi ke generasi untuk kesehatan.
        Jamu dibuat dari bahan-bahan alami, berupa bagian dari tumbuh-tumbuhan seperti rimpang (akar-akaran), daun-daunan, kulit batang, dan buah. Ada juga menggunakan bahan dari tubuh hewan, seperti empedu kambing, empedu ular, atau tangkur buaya. Seringkali kuning telur ayam kampung juga dipergunakan untuk tambahan campuran pada jamu gendong. 

    </section><!-- End About Section -->

    <!-- ======= Features Section ======= -->
    <section class="features" id="features" style="background-color:orange;>

      <div class="container">
        <h2 class="text-center">
          Manfaat Jamu
        </h2>

        <ul>
          <li>Membantu mempertahankan imunitas</a></li>
          <li>Pereda Nyeri</a></li>
          <li>Obat Hipertensi</a></li>
          <li>Obat Asam Urat</a></li>
          <li>Penambah Nafsu Makan</li>
          <li>Kesehatan dan Kecantikan</a></li>
        </ul>

    </section><!-- End Features Section -->

    <!-- ======= Call to Action Section ======= -->
    <section class="cta">
      <div class="container">
        <div class="row">
          <div class="col-lg-9 col-sm-12 text-lg-start text-center">
            <h2>
              Call to Action Section
            </h2>

            <p>
              Lorem ipsum dolor sit amet, nec ad nisl mandamus imperdiet, ut corpora cotidieque cum. Et brute iracundia his, est eu idque dictas gubergren
            </p>
          </div>

          <div class="col-lg-3 col-sm-12 text-lg-right text-center">
            <a class="btn btn-ghost" href="#">Buy This Template</a>
          </div>
        </div>
      </div>
    </section><!-- End Call to Action Section -->

    <!-- ======= Portfolio Section ======= -->
    <section class="portfolio" id="portfolio">

      <div class="container text-center">
        <h2>
          Portfolio
        </h2>

        <p>
          Voluptua scripserit per ad, laudem viderer sit ex. Ex alia corrumpit voluptatibus usu, sed unum convenire id. Ut cum nisl moderatius, Per nihil dicant commodo an.
        </p>
      </div>

      <div class="portfolio-grid">
        <div class="row">
          <div class="col-lg-3 col-sm-6 col-xs-12">
            <div class="card card-block">
              <a href="assets/img/porf-1.jpg" class="portfolio-lightbox" data-gallery="portfolioGallery"><img alt="" src="assets/img/porf-1.jpg">
                <div class="portfolio-over">
                  <div>
                    <h3 class="card-title">
                      The Dude Rockin'
                    </h3>

                    <p class="card-text">
                      Lorem ipsum dolor sit amet, eu sed suas eruditi honestatis.
                    </p>
                  </div>
                </div>
              </a>
            </div>
          </div>

          <div class="col-lg-3 col-sm-6 col-xs-12">
            <div class="card card-block">
              <a href="assets/img/porf-2.jpg" class="portfolio-lightbox" data-gallery="portfolioGallery"><img alt="" src="assets/img/porf-2.jpg">
                <div class="portfolio-over">
                  <div>
                    <h3 class="card-title">
                      The Dude Rockin'
                    </h3>

                    <p class="card-text">
                      Lorem ipsum dolor sit amet, eu sed suas eruditi honestatis.
                    </p>
                  </div>
                </div>
              </a>
            </div>
          </div>

          <div class="col-lg-3 col-sm-6 col-xs-12">
            <div class="card card-block">
              <a href="assets/img/porf-3.jpg" class="portfolio-lightbox" data-gallery="portfolioGallery"><img alt="" src="assets/img/porf-3.jpg">
                <div class="portfolio-over">
                  <div>
                    <h3 class="card-title">
                      The Dude Rockin'
                    </h3>

                    <p class="card-text">
                      Lorem ipsum dolor sit amet, eu sed suas eruditi honestatis.
                    </p>
                  </div>
                </div>
              </a>
            </div>
          </div>

          <div class="col-lg-3 col-sm-6 col-xs-12">
            <div class="card card-block">
              <a href="assets/img/porf-4.jpg" class="portfolio-lightbox" data-gallery="portfolioGallery"><img alt="" src="assets/img/porf-4.jpg">
                <div class="portfolio-over">
                  <div>
                    <h3 class="card-title">
                      The Dude Rockin'
                    </h3>

                    <p class="card-text">
                      Lorem ipsum dolor sit amet, eu sed suas eruditi honestatis.
                    </p>
                  </div>
                </div>
              </a>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-lg-3 col-sm-6 col-xs-12">
            <div class="card card-block">
              <a href="assets/img/porf-5.jpg" class="portfolio-lightbox" data-gallery="portfolioGallery"><img alt="" src="assets/img/porf-5.jpg">
                <div class="portfolio-over">
                  <div>
                    <h3 class="card-title">
                      The Dude Rockin'
                    </h3>

                    <p class="card-text">
                      Lorem ipsum dolor sit amet, eu sed suas eruditi honestatis.
                    </p>
                  </div>
                </div>
              </a>
            </div>
          </div>

          <div class="col-lg-3 col-sm-6 col-xs-12">
            <div class="card card-block">
              <a href="assets/img/porf-6.jpg" class="portfolio-lightbox" data-gallery="portfolioGallery"><img alt="" src="assets/img/porf-6.jpg">
                <div class="portfolio-over">
                  <div>
                    <h3 class="card-title">
                      The Dude Rockin'
                    </h3>

                    <p class="card-text">
                      Lorem ipsum dolor sit amet, eu sed suas eruditi honestatis.
                    </p>
                  </div>
                </div>
              </a>
            </div>
          </div>

          <div class="col-lg-3 col-sm-6 col-xs-12">
            <div class="card card-block">
              <a href="assets/img/porf-7.jpg" class="portfolio-lightbox" data-gallery="portfolioGallery"><img alt="" src="assets/img/porf-7.jpg">
                <div class="portfolio-over">
                  <div>
                    <h3 class="card-title">
                      The Dude Rockin'
                    </h3>

                    <p class="card-text">
                      Lorem ipsum dolor sit amet, eu sed suas eruditi honestatis.
                    </p>
                  </div>
                </div>
              </a>
            </div>
          </div>

          <div class="col-lg-3 col-sm-6 col-xs-12">
            <div class="card card-block">
              <a href="assets/img/porf-8.jpg" class="portfolio-lightbox" data-gallery="portfolioGallery"><img alt="" src="assets/img/porf-8.jpg">
                <div class="portfolio-over">
                  <div>
                    <h3 class="card-title">
                      The Dude Rockin'
                    </h3>

                    <p class="card-text">
                      Lorem ipsum dolor sit amet, eu sed suas eruditi honestatis.
                    </p>
                  </div>
                </div>
              </a>
            </div>
          </div>
        </div>
      </div>
    </section><!-- End Portfolio Section -->